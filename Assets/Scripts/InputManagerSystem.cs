﻿using System;
using UnityEngine;
using Unity.Entities;

public class InputManagerSystem : ComponentSystem
{
    public static event Action<Vector2> OnClick;
    public static event Action OnEscape;

    private Camera activeCamera = null;

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        activeCamera = Camera.main;
    }

    protected override void OnUpdate()
    {
        if (Input.GetMouseButtonDown(0))
            OnClick?.Invoke(activeCamera.ScreenToWorldPoint(Input.mousePosition));

        if (Input.GetKeyDown(KeyCode.Escape))
            OnEscape?.Invoke();
    }
}