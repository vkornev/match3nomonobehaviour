﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Board
{
    public event Action<int, bool> OnMatch;
    public event Action OnFail;
    public event Action OnReset;

    private readonly Color[] availableColors =
        new Color[]
        {
            new Color(1f, 0f, 0f),
            new Color(0f, 1f, 0f),
            new Color(0f, 0f, 1f),
            new Color(1f, 1f, 0f),
            new Color(1f, 0f, 1f),
            new Color(0f, 1f, 1f)
        };
    private readonly int xSize;
    private readonly int ySize;
    private Cell[,] cells = null;
    private Sprite sprite = null;

    private Cell lastClickedCell = null;

    private bool replay = false;
    private bool gravity = true;

    public Board(int xSize, int ySize)
    {
        this.xSize = xSize;
        this.ySize = ySize;

        InputManagerSystem.OnClick += Click;

        sprite = Resources.Load<Sprite>("cell");

        RestartBoard();
    }

    ~Board()
    {
        InputManagerSystem.OnClick -= Click;
    }

    public void Continue()
    {
        if (gravity) ShiftCellsDown();
        else ShiftCellsUp();

        Repopulate();
        ProcessBoard(false);
    }

    public void EndGame()
    {
        replay = true;
    }

    private void RestartBoard()
    {
        if (cells != null)
            foreach (Cell c in cells)
                GameObject.Destroy(c.MyObject);

        lastClickedCell = null;

        cells = new Cell[xSize, ySize];

        for (int x = 0; x < xSize; x++)
            for (int y = 0; y < ySize; y++)
            {
                Color rndColor = availableColors[Random.Range(0, availableColors.Length)];
                cells[x, y] = new Cell(new Vector2Int(x, y), rndColor, sprite, false);
            }

        OnReset?.Invoke();
        InitializeBoard();
    }

    private void Click(Vector2 position)
    {
        if (replay)
        {
            replay = false;
            RestartBoard();
            return;
        }

        if (position.x < -.5f || position.y < -.5f || position.x > (xSize - .5f) || position.y > (ySize - .5f))
            return;

        Vector2 precisePosition = new Vector2(Mathf.RoundToInt(Mathf.Abs(position.x)), Mathf.RoundToInt(Mathf.Abs(position.y)));
        ProcessClick(precisePosition);
    }

    private void ProcessClick(Vector2 precisePosition)
    {
        Cell cellClicked = cells[(int)precisePosition.x, (int)precisePosition.y];

        if (lastClickedCell == null ||
            (cellClicked.Position - lastClickedCell.Position).sqrMagnitude > 1)
        {
            lastClickedCell = cellClicked;
            return;
        }

        SwapCells(lastClickedCell, cellClicked);
        lastClickedCell = null;

        ProcessBoard(true);
    }

    private void SwapCells(Cell cell1, Cell cell2)
    {
        Cell tempCell = cells[cell1.Position.x, cell1.Position.y];
        cells[cell1.Position.x, cell1.Position.y] = cells[cell2.Position.x, cell2.Position.y];
        cells[cell2.Position.x, cell2.Position.y] = tempCell;

        Vector2Int temp = cell1.Position;
        cell1.SetPosition(cell2.Position);
        cell2.SetPosition(temp);
    }

    private void InitializeBoard()
    {
        bool goDeep = true;
        int moveScore = 0;
        while (goDeep)
        {
            CheckMatches(out int matches, out bool switchGravity);
            if (switchGravity) gravity = !gravity;

            if (gravity) ShiftCellsDown();
            else ShiftCellsUp();

            Repopulate();
            moveScore += matches;
            if (matches == 0) goDeep = false;
        }
    }

    private void ProcessBoard(bool playerActivated)
    {
        CheckMatches(out int matches, out bool switchGravity);
        if (switchGravity) gravity = !gravity;
        if (matches > 0)
            OnMatch?.Invoke(matches, playerActivated);
        else if (playerActivated)
            OnFail?.Invoke();
    }

    private void CheckMatches(out int matches, out bool switchGravity)
    {
        matches = 0;
        switchGravity = false;
        for (int x = 0; x < xSize; x++)
            for (int y = 0; y < ySize; y++)
            {
                if (cells[x, y] == null || cells[x, y].MyObject == null || cells[x, y].IsMatch) continue;

                List<Cell> checkedNeighBors = new List<Cell>();
                CheckNeighbors(x, y, ref checkedNeighBors);

                if (checkedNeighBors.Count < 3) continue;

                matches += checkedNeighBors.Count;

                for (int k = 0; k < checkedNeighBors.Count; k++)
                {
                    checkedNeighBors[k].SetMatched(true);
                    if (checkedNeighBors[k].IsGravitySwitcher)
                        switchGravity = true;
                }
            }
    }

    private void CheckNeighbors(int x, int y, ref List<Cell> checkedNeighBors)
    {
        checkedNeighBors.Add(cells[x, y]);

        Color currentTileColor = cells[x, y].Color;

        int leftX = x - 1;
        Cell leftTile = leftX >= 0 ? cells[leftX, y] : null;
        int rightX = x + 1;
        Cell rightTile = rightX < xSize ? cells[rightX, y] : null;
        int topY = y + 1;
        Cell topTile = topY < ySize ? cells[x, topY] : null;
        int bottomY = y - 1;
        Cell bottomTile = bottomY >= 0 ? cells[x, bottomY] : null;

        if (leftTile != null && !checkedNeighBors.Contains(leftTile) && leftTile.Color == currentTileColor)
            CheckNeighbors(leftX, y, ref checkedNeighBors);
        if (rightTile != null && !checkedNeighBors.Contains(rightTile) && rightTile.Color == currentTileColor)
            CheckNeighbors(rightX, y, ref checkedNeighBors);
        if (topTile != null && !checkedNeighBors.Contains(topTile) && topTile.Color == currentTileColor)
            CheckNeighbors(x, topY, ref checkedNeighBors);
        if (bottomTile != null && !checkedNeighBors.Contains(bottomTile) && bottomTile.Color == currentTileColor)
            CheckNeighbors(x, bottomY, ref checkedNeighBors);
    }

    private void ShiftCellsDown()
    {
        for (int x = 0; x < xSize; x++)
        {
            Queue<int> yEmptyQ = new Queue<int>();
            for (int y = 0; y < ySize; y++)
            {
                ShiftCell(x, y, ref yEmptyQ);
            }
        }
    }

    private void ShiftCellsUp()
    {
        for (int x = 0; x < xSize; x++)
        {
            Queue<int> yEmptyQ = new Queue<int>();
            for (int y = ySize - 1; y >= 0; y--)
            {
                ShiftCell(x, y, ref yEmptyQ);
            }
        }
    }

    private void ShiftCell(int x, int y, ref Queue<int> yEmptyQ)
    {
        if (cells[x, y] != null && cells[x, y].IsMatch)
        {
            if (cells[x, y].MyObject) GameObject.Destroy(cells[x, y].MyObject);
            cells[x, y] = null;
        }
        else if (cells[x, y] != null && yEmptyQ.Count > 0)
        {
            int yEmpty = yEmptyQ.Dequeue();
            cells[x, yEmpty] = cells[x, y];
            cells[x, yEmpty].SetPosition(new Vector2Int(x, yEmpty));
            cells[x, y] = null;
        }

        if (cells[x, y] == null)
            yEmptyQ.Enqueue(y);
    }

    private void Repopulate()
    {
        bool isGravitySwitcher = true;
        for (int x = 0; x < xSize; x++)
            for (int y = 0; y < ySize; y++)
            {
                if (cells[x, y] != null) continue;

                Color rndColor = availableColors[Random.Range(0, availableColors.Length)];
                Cell newCell = new Cell(new Vector2Int(x, y), rndColor, sprite, isGravitySwitcher ? true : false);
                isGravitySwitcher = false;
                cells[x, y] = newCell;
            }
    }
}
