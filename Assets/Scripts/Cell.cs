﻿using UnityEngine;
using System;

[Serializable]
public class Cell
{
    public bool IsMatch { get; private set; }
    public bool IsGravitySwitcher { get; private set; }
    public Color Color { get; private set; }
    public Vector2Int Position { get; private set; }
    public GameObject MyObject { get; private set; }

    public Cell(Vector2Int position, Color color, Sprite sprite, bool isGravitySwitcher)
    {
        IsMatch = false;
        IsGravitySwitcher = isGravitySwitcher;

        Color = color;

        MyObject = new GameObject("cell", typeof(SpriteRenderer));
        SetPosition(position);
        SpriteRenderer spriteRenderer = MyObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = sprite;
        spriteRenderer.color = Color;
    }

    public void SetPosition(Vector2Int newPosition)
    {
        Position = newPosition;
        MyObject.transform.position = new Vector3(Position.x, Position.y, 0);
    }

    public void SetMatched(bool val)
    {
        IsMatch = val;
        MyObject.SetActive(!val);
    }
}
