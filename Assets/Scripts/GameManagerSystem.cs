﻿using UnityEngine;
using Unity.Entities;
using TMPro;

public class GameManagerSystem : ComponentSystem
{
    private const float afterMatchPause = .5f;

    private Board board = null;
    private TextMeshProUGUI statText = null;
    private long score = 0;
    private float matchTimer = 0f;

    protected override void OnCreate()
    {
        base.OnCreate();

        statText = Object.FindObjectOfType<TextMeshProUGUI>();

        board = new Board(GameSettings.Width, GameSettings.Height);
        board.OnMatch += ScoreMatch;
        board.OnFail += FailGame;
        board.OnReset += ResetBoard;
    }

    protected override void OnStartRunning()
    {
        base.OnStartRunning();

        InputManagerSystem.OnEscape += Application.Quit;
    }

    protected override void OnStopRunning()
    {
        base.OnStopRunning();

        InputManagerSystem.OnEscape -= Application.Quit;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        board.OnMatch -= ScoreMatch;
        board.OnFail -= FailGame;
        board.OnReset -= ResetBoard;
    }

    protected override void OnUpdate()
    {
        if (matchTimer <= 0f) return;

        matchTimer -= Time.DeltaTime;

        if (matchTimer <= 0f)
            board.Continue();
    }

    private void ScoreMatch(int numberOfMatched, bool playerActivated)
    {
        score += numberOfMatched * GameSettings.ScoreValue;
        statText.text = $"Points: {score}";
        matchTimer = afterMatchPause;
    }

    private void FailGame()
    {
        statText.text = $"Game ended. Points gained: {score}. Click to replay";
        board.EndGame();
        score = 0;
    }

    private void ResetBoard()
    {
        statText.text = string.Empty;
    }
}
