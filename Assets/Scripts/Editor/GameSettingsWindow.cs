﻿using UnityEngine;
using UnityEditor;

public class GameSettingsWindow : EditorWindow
{
    [MenuItem("Match3/Game Settings")]
    static void Init()
    {
        GameSettingsWindow window = (GameSettingsWindow)GetWindow(typeof(GameSettingsWindow));
        window.Show();
    }

    void OnGUI()
    {
        GameSettings.Width = EditorGUILayout.IntField("Width", GameSettings.Width);
        GameSettings.Height = EditorGUILayout.IntField("Height", GameSettings.Height);
        GameSettings.ScoreValue = EditorGUILayout.IntField("ScoreValue", GameSettings.ScoreValue);

        GUI.backgroundColor = Color.red;
        if (GUILayout.Button("Reset", GUILayout.Width(100), GUILayout.Height(30)))
        {
            GameSettings.Width = 6;
            GameSettings.Height = 6;
            GameSettings.ScoreValue = 1;
        }
    }
}
