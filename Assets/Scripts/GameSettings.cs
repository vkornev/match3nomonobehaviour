﻿#if UNITY_EDITOR
using UnityEditor;
#endif

public class GameSettings
{
    public static int Width
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetInt("Width", 6);
#else
            return 6;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetInt("Width", value);
#endif
        }
    }

    public static int Height
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetInt("Height", 6);
#else
            return 6;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetInt("Height", value);
#endif
        }
    }

    public static int ScoreValue
    {
        get
        {
#if UNITY_EDITOR
            return EditorPrefs.GetInt("ScoreValue", 1);
#else
            return 1;
#endif
        }

        set
        {
#if UNITY_EDITOR
            EditorPrefs.SetInt("ScoreValue", value);
#endif
        }
    }
}